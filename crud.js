let http = require("http");

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];

http.createServer((request, response) => {
	if(request.url == '/users' && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}

	if(request.url == '/users' && request.method == "POST") {
		// declare a placeholder variable in order to be re-assigned later on. This variable will be assigned the date inside the body from Postman.
		let request_body = '';

		// When data is detected, run a function that assignes that data to the empty request_body variable
		request.on('data', (data) => {
			request_body += data;
			console.log(data);
			console.log(request_body);
		}); 

		// Before the request ends, we want to add the new data to our existing users array of objects
		request.on('end', () => {
			console.log(typeof request_body);

			// Convert the request_body from JSON to a JS Object, then assign that converted value back to the request_body variable.
			request_body = JSON.parse(request_body);

			// Declare a new_user variable signifying the new data that came from the Postman body
			let new_directory = {
				"name" : request_body.name,
				"email": request_body.email
			}

			// Add the new_user variable (object) into the users array.
			directory.push(new_directory);
			console.log(directory);

			// Write the headers for the response. Make sure the content is 'application/json' since we are writing/returning a JSON
			response.writeHead(200, {'Conten-Type': 'application/json'})
			response.write(JSON.stringify(new_directory));
			response.end();
		}); 
	}
}).listen(4000);

console.log('Server running at localhost:4000');