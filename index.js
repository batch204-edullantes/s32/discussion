let http = require("http");

// GHTTP is a protocol that allows the fetching of resources such as HTML documents

http.createServer((request, response) => {
	if(request.url == '/items' && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from the database');
	}
}).listen(4000);

// A port is a virtual point where network connections atart and end

// The server will be assigned to port 4000 via the "listen(4000)"

console.log(`Server running at localhost:4000`);

//  When server is running, console will print the message